<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'niller_db');

/** MySQL database username */
define('DB_USER', 'homestead');

/** MySQL database password */
define('DB_PASSWORD', 'secret');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'V7x-tG-?Avvkr8HL* Y?J,QxE5QKX_zs0-[S!:+I`k_&O)4M{{]-e?|:@4^[[#kP');
define('SECURE_AUTH_KEY',  ',I!@kYAZ>dKhgG77u`i[i|>#MwXs~N+{5:2:@j[2Bv+Ts`-(PM~I0^n+0<:4I:Lj');
define('LOGGED_IN_KEY',    '-+?9b!}hW%f#Bu[G^<eEJ,BYo-?{03vO*)Y,dfaTLlOcl-69HqN2}^5?G!dx4XfN');
define('NONCE_KEY',        'RFb:nB ]6,qG{b7eP^)=0~)G9DMW{p2V?;uA8/GySU(l.p?mLxSM#lT=WKew@v>|');
define('AUTH_SALT',        'EXyy&vyU!St(^hmY.ls<qUA1Hk5S)zf?jg$HI;HsAwFn?FG|<rTKj>91}t!(;E@V');
define('SECURE_AUTH_SALT', 'r;9qE3DY:0Tn2G!=[]|)5,]IH/M!%Xha;4@2>NH[Ty]IR@w^5)Ud:8pLSS}scD1z');
define('LOGGED_IN_SALT',   'Fu31(sI*uzH7<<6(h/O9]x[|rY~ipj%Qj%a-v1jQqs(IM?+}U=oNaj$K3eMk/-0Q');
define('NONCE_SALT',       'zd/!x7__S-qmibM6);iZ2!n+X$`ZlF$FNuCoweZd(U4$8,Z>k4/<TuH<?xGsYLx6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
