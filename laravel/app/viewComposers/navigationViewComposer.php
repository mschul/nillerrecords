<?php namespace Niller\ViewComposers;

use Illuminate\Support\Collection;  
use Illuminate\View\View;
use URL;
use Request;

class NavigationViewComposer {

  public function disabledTag($site)
  {
    return Request::is($site.'*') ? 'disabled' : 'enabled';
  }

  public function compose(View $view) {

    $menu = new Collection;

    $menu->push((object)['title' => 'zuhause', 'link' => URL::to('/'), 'state' => $this->disabledTag('/')]);
    $menu->push((object)['title' => 'niller', 'link' => URL::to('niller'), 'state' => $this->disabledTag('niller')]);
    $menu->push((object)['title' => 'artists', 'link' => URL::to('artists'), 'state' => $this->disabledTag('artists')]);
    $menu->push((object)['title' => 'laden', 'link' => URL::to('laden'), 'state' => $this->disabledTag('laden')]);
    $menu->push((object)['title' => 'kontakt', 'link' => URL::to('contact'), 'state' => $this->disabledTag('contact')]);

    $view->menu = $menu;

    
  }

}
