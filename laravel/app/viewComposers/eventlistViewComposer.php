<?php namespace Niller\ViewComposers;

use Illuminate\Support\Collection;
use Illuminate\View\View;

use Niller\Storage\EventRepository;

class EventlistViewComposer {
  
  protected $events;
  public function __construct(EventRepository $events)
  {
    $this->events = $events;
  }
  
  public function compose(View $view) {
    $view->eventlist = $this->events->indexFuture();
  }
}