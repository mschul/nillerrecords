<?php namespace Niller\ViewComposers;

use Illuminate\Support\Collection;
use Illuminate\View\View;

use Niller\Storage\PostRepository;

class TaglistViewComposer {
  
  protected $posts;
  public function __construct(PostRepository $posts)
  {
    $this->posts = $posts;
  }
  
  public function compose(View $view) {
    $view->taglist = $this->posts->getTags();
  }
}
