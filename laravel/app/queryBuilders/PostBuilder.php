<?php

/**
 * PostBuilder
 */

use Illuminate\Database\Eloquent\Builder;

class PostBuilder extends Builder
{
    /**
     * Get only posts with a custom status
     * 
     * @param string $postStatus
     * @return \PostBuilder
     */
    public function status($postStatus)
    {
        return $this->where('post_status', $postStatus);
    }

    /**
     * Get only published posts
     * 
     * @return \PostBuilder
     */
    public function published()
    {
        return $this->status('publish');
    }

    /**
     * Get only posts from a custo post type
     * 
     * @param string $type
     * @return \PostBuilder
     */
    public function type($type)
    {
        return $this->where('post_type', $type);
    }
    
    public function multitype($types)
    {
	return $this->where(function($query) use ($types)
		    {
			 foreach($types as $type)
			     $query->orWhere('post_type', $type);
		    }
	       );
    }

    public function taxonomy($taxonomy, $term)
    {
        return $this->whereHas('taxonomies', function($query) use ($taxonomy, $term) {
            $query->where('taxonomy', $taxonomy)->whereHas('term', function($query) use ($term) {
                $query->where('slug', $term);
            });
        });
    }

    /**
     * Get only posts with a specific slug
     * 
     * @param string slug
     * @return \PostBuilder
     */
    public function slug($slug)
    {
        return $this->where('post_name', $slug);
    }

    /**
     * Overrides the paginate() method to a custom and simple way.
     * 
     * @param int $perPage
     * @param int $currentPage
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function paginate($perPage = 10, $currentPage = 1)
    {
        $count = $this->count();
	return $this->skip($perPage * ($currentPage - 1))->take($perPage)->get();
    }
    
    public function inDateInterval($minDate, $maxDate)
    {
	return $this->where('post_date', "<=", $maxDate)->where('post_date', ">=", $minDate);
    }
}