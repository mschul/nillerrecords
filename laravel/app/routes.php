<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::pattern('id', '\d+');
Route::pattern('slug', '[a-z0-9-]+');
Route::pattern('uuid', '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}');
Route::pattern('yyyy', '[0-9]{4}');
Route::pattern('dd', '[0-9]{2}');
Route::pattern('mm', '[0-9]{2}');

Route::get('/', array('uses' => 'PostController@index'));
Route::get('/{yyyy}', array('uses' => 'PostController@indexY'));
Route::get('/{mm}/{yyyy}', array('uses' => 'PostController@indexMY'));
Route::get('/{dd}/{mm}/{yyyy}', array('uses' => 'PostController@indexDMY'));
Route::get('/{dd}/{mm}/{yyyy}/{name}', array('uses' => 'PostController@view'));
Route::get('/tag/{name}', array('uses' => 'PostController@indexByTag'));

Route::get('events', array('uses' => 'EventController@index'));
Route::get('events/{id}', array('uses' => 'EventController@view'));

Route::get('artists', array('uses' => 'ArtistController@index'));
Route::get('artists/{name}', array('uses' => 'ArtistController@view'));

Route::get('niller', array('uses' => 'NillerController@index'));
Route::get('niller/{name}', array('uses' => 'NillerController@view'));

Route::get('laden', array('uses' => 'LadenController@index'));
Route::get('contact', function() { return View::make('contact'); });
Route::get('notice', function() { return View::make('notice'); });
Route::get('admin', function() 	 { return Redirect::to('wordpress/wp-login.php'); });
