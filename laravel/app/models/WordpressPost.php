<?php namespace Niller\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Str;
use PostBuilder;

class WordpressPost extends Eloquent
{
    const CREATED_AT = 'post_date';
    const UPDATED_AT = 'post_modified';

    protected $table = 'wp_posts';
    protected $primaryKey = 'ID';
    protected $with = array('meta');

    /**
     * Meta data relationship
     *
     * @return PostMetaCollection
     */
    public function meta()
    {
        return $this->hasMany('Niller\Models\PostMeta', 'post_id');
    }

    public function fields()
    {
        return $this->meta();
    }

    /**
     * Taxonomy relationship
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function taxonomies()
    {
        return $this->belongsToMany('Niller\Models\TermTaxonomy', 'term_relationships', 'object_id', 'term_taxonomy_id');
    }

    /**
     * Comments relationship
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function comments()
    {
        return $this->hasMany('Niller\Models\Comment', 'comment_post_ID');
    }

    /**
     * Get attachment
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function attachment()
    {
        return $this->hasMany('Niller\Models\WordpressPost', 'post_parent')->where('post_type', 'attachment');
    }


    /**
     * Get revisions from post
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function revision()
    {
        return $this->hasMany('Niller\Models\WordpressPost', 'post_parent')->where('post_type', 'revision');
    }

    /**
     * Overriding newQuery() to the custom PostBuilder with some interesting methods
     *
     * @param bool $excludeDeleted
     * @return PostBuilder
     */
    public function newQuery($excludeDeleted = true)
    {
        $builder = new PostBuilder($this->newBaseQueryBuilder());
        $builder->setModel($this)->with($this->with);

        if (isset($this->postType) and $this->postType) {
            $builder->type($this->postType);
        }

        if ($excludeDeleted and $this->softDelete) {
            $builder->whereNull($this->getQualifiedDeletedAtColumn());
        }

        return $builder;
    }

    /**
     * Magic method to return the meta data like the post original fields
     *
     * @param string $key
     * @return string
     */
    public function __get($key)
    {
        if (!isset($this->$key)) {
            if (isset($this->meta()->get()->$key)) {
                return $this->meta()->get()->$key;
            }
        }

        return parent::__get($key);
    }

    public function save(array $options = array())
    {
        if (isset($this->attributes[$this->primaryKey])) {
            $this->meta->save($this->attributes[$this->primaryKey]);
        }

        return parent::save($options);
    }


    public function content()
    {
        $content = apply_filters('the_content', $this->post_content);
        return str_replace(']]>', ']]&gt;', $content);
    }
    
    public function excerpt($words = -1)
    {
        $content = get_extended($this->post_content)['main'];
        $content = strip_tags($content);
        $content = preg_replace('/\[.*\]/', '', $content); 
        return $words > 0 ? Str::words($content, $words) : $content;
    }
    public function extended()
    {
        $content = get_extended($this->post_content)['extended'];
        $content = apply_filters('the_content', $content);
        return str_replace(']]>', ']]&gt;', $content);
    }
    
    public function has_thumbnail()
    {
        return has_post_thumbnail($this->ID);
    }
    public function thumbnail($size)
    {
        return get_the_post_thumbnail($this->ID, $size);
    }
	public function thumbnail_url()
	{
	    return wp_get_attachment_image_src(get_post_thumbnail_id($this->ID), 'large' )[0];
	}
    public function title($length = -1)
    {
        return $length > 0 ? Str::limit($this->post_title, $length) : $this->post_title;
    }
    public function unique_name()
    {
        return date("d/m/Y", strtotime($this->post_date)) . '/' . $this->post_title;
    }
    public function post_url()
    {
        if($this->post_type == 'post')
            return url($this->unique_name());
        else if($this->post_type == 'events')
            return url('events/'.$this->ID);
        else if($this->post_type == 'artist')
            return url('artists/'.$this->post_title);
        else
            return url('/');
    }
}