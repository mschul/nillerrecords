@extends('layout')
@section('header')
<title>Niller Records - Impressum</title>
<meta name="description" content="Ida malt gerne, Phillie fährt seine Platten am liebsten mit dem Fahrrad durch die Gegend. Sie supporten Künstler, Produzenten, DJs und Plattensammler.">
<meta name="og:title" content="Is klar ne.">
<meta name="og:site-name" content="Niller Records">
<meta name="og:image" content="http://www.nillerrecords.com/images/about.png">
<meta name="og:type" content="blog">
<meta name="og:locale" content="de-DE">
<meta name="og:description" content="Ida malt gerne, Phillie fährt seine Platten am liebsten mit dem Fahrrad durch die Gegend und Leika steht auf Enten. Außerdem lieben und sammeln die drei Musik. Seit 2013 versorgen sie auch den Rest der Welt damit und supporten dabei verschiedene Künstler, Produzenten, DJs und Plattensammler.">
@stop

@section('content')
    <h2 class="center">Impressum</h2>
	<br/>
	<p class="center">
Philipp Rautzenberg<br/>
Steinstr. 23<br/>
76133 Karlsruhe<br/>
<br/>
Tel.: 0721 / 33555380<br/>
<br/>
Geschäftsführer:<br/>
Philipp Rautzenberg<br/>
<br/>
Gestaltung:<br/>
Ida Grosse<br/>
<br/>
Website:<br/>
Max Schuler
	</p>
	<br/>
<h2 class="center">Rechtshinweis</h2>
	<br/>
<p class="disclaimer">
Alle in unseren Internetseiten enthaltenen Angaben wurden sorgfältig recherchiert. Für Richtigkeit, Vollständigkeit und Aktualität kann keine Haftung übernommen werden.
Trotz sorgfältiger inhaltlicher Kontrolle übernehmen wir keine Haftung für die Inhalte externer Links. Für den Inhalt der verlinkten Seiten sind ausschließlich deren Betreiber verantwortlich.
</p>
<br/>
<h2 class="center">Google Analytics Datenschutzerklärung</h2>
	<br/>
<p class="disclaimer">
Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch das Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Website, wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt. Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem Sie das unter dem folgenden Link (<a href=http://tools.google.com/dlpage/gaoptout?hl=de>http://tools.google.com/dlpage/gaoptout?hl=de</a>) verfügbare Browser-Plugin herunterladen und installieren.
</p>
@stop