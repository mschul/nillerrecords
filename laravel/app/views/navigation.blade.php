<ul id="navigation">
@foreach($menu->all() as $item)
    @if(isset($item->link) && isset($item->state) && $item->state=="disabled")
      <li>
        <h2><a href="{{ $item->link }}" class="element-link-3">{{$item->title}}</a></h2>
      </li>
    @elseif(isset($item->link))
	  <li>
        <h2><a href="{{ $item->link }}" class="element-link-2">{{$item->title}}</a></h2>
      </li>
    @endif
@endforeach
</ul>