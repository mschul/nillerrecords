@extends('layout')
@section('header')
<title>Niller Records - {{ $post->post_title }}</title>
<meta name="description" content="{{ $post->excerpt(15) }}">
<meta name="og:title" content="{{ $post->post_title }}">
<meta name="og:site-name" content="Niller Records">
@if ($post->has_thumbnail())
<meta name="og:image" content="{{ $post->thumbnail_url() }}">
@else
<meta name="og:image" content="http://www.nillerrecords.com/images/about.png">
@endif
<meta name="og:type" content="article">
<meta name="og:locale" content="de-DE">
<meta name="og:description" content="{{ $post->excerpt(30) }}">
@stop

@section('content')
    <div class="post-entry">
    <h2>{{ $post->post_title }}</h2>
    <p class="date">{{ date("j. m. Y",strtotime($post->post_date)) }}</p>
	{{ $post->content() }}
	
    <div class="clear"></div>
    </div>
    <!-- start fancybox -->
    <script>
	$(document).ready(function() {
		$("a:has(img)").attr('rel', 'gallery').fancybox({
			'openEffect'	: 'elastic',
			'closeEffect'	: 'none',
			'beforeLoad' : function() { this.title = ""; }
		});
	});
    </script>
@stop