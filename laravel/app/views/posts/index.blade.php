@extends('layout')

@section('header')
<title>Niller Records</title>
<meta name="description" content="Ida malt gerne, Phillie fährt seine Platten am liebsten mit dem Fahrrad durch die Gegend. Sie supporten Künstler, Produzenten, DJs und Plattensammler.">
<meta name="og:title" content="Is klar ne.">
<meta name="og:site-name" content="Niller Records">
<meta name="og:image" content="http://www.nillerrecords.com/images/about.png">
<meta name="og:type" content="blog">
<meta name="og:locale" content="de-DE">
<meta name="og:description" content="Ida malt gerne, Phillie fährt seine Platten am liebsten mit dem Fahrrad durch die Gegend und Leika steht auf Enten. Außerdem lieben und sammeln die drei Musik. Seit 2013 versorgen sie auch den Rest der Welt damit und supporten dabei verschiedene Künstler, Produzenten, DJs und Plattensammler.">
@stop

@section('content')
    <div id="scroll">
    <ul>
@foreach($posts as $post)
    <li class="post-entry">
	<a class="element-link" href="{{ $post->post_url() }}">
        <div class="post-thumb">
            @if ($post->has_thumbnail())
                {{ $post->thumbnail(array(300,200)) }}
            @else
                {{ HTML::image('images/about.png', $alt="No Image", $attributes = array("width" => 300, "height" => 200)) }}
            @endif
        </div>
    <h2>
        {{ $post->post_title }}
    </h2>
    </a>
    <p class="date">{{ date("j. m. Y",strtotime($post->post_date)) }}</p>
	<p>
	{{ $post->excerpt(40) }}
	</p>
    <div class="clear"></div>
    </li>
@endforeach
    </ul>
    {{ $posts->links() }}
    </div>
    
    <!-- start jscroller -->
    <script>    
	$('#scroll').jscroll({
	    autoTrigger: true,
	    nextSelector: '.pagination li.active + li a',
	    contentSelector: '#scroll',
	    padding: 100,
	    loadingHtml: ''
	});
    </script>
@stop