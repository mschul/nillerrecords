    <h2 class="center">{{ $about->post_title }}</h2>
	{{ HTML::image('images/about.png') }}
    {{ $about->content() }}
    <ul id="site-icons">
	<li class="fb-icon"><a href="https://www.facebook.com/nillerrecords"></a></li>
	<li class="sc-icon"><a href="https://soundcloud.com/nillerrecords"></a></li>
	<li class="insta-icon"><a href="http://instagram.com/nillerrecords"></a></li>
	</ul>
    <h2 class="center">Veranstaltungen</h2>
    <ul class="event-list">
@foreach($eventlist as $event)
    <li>
    <a class="element-link" href="{{ url('events/'.$event->ID) }}">
    <b>{{ $event->post_title }}</b>
    <em>{{ date("j. F, Y",strtotime($event->meta->event_date)) }}</em>
    </a>
    </li>
@endforeach
    </ul>
    <a href="{{ url('events') }}" class="element-link-2">WEITERE VERANSTALTUNGEN</a>
    <div class="spacer"></div>
    <h2 class="center">Themen</h2>
    <ul class="tag-list">
@foreach($taglist as $tag)
    <li>
    <a class="element-link" href="{{ url('tag/'.$tag->slug) }}">
    <b>{{ $tag->name }}</b>
    <em>({{ $tag->count }})</em>
    </a>
    </li>
@endforeach
    </ul>
    <div class="spacer"></div>
    <a href="{{ url('notice') }}" class="element-link-4">IMPRESSUM</a>
