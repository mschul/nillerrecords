@extends('layout')

@section('header')
<title>Niller Records - {{ $artist->post_title }}</title>
<meta name="description" content="{{ $artist->excerpt(15) }}">
<meta name="og:title" content="{{ $artist->post_title }}">
<meta name="og:site-name" content="Niller Records">
@if ($artist->has_thumbnail())
<meta name="og:image" content="{{ $artist->thumbnail_url() }}">
@else
<meta name="og:image" content="http://www.nillerrecords.com/images/about.png">
@endif
<meta name="og:type" content="blog">
<meta name="og:locale" content="de-DE">
<meta name="og:description" content="{{ $artist->excerpt(30) }}">
@stop

@section('content')
	<h2>
		{{ $artist->post_title }}
	</h2>
	{{ $artist->content() }}
    <!-- start fancybox -->
    <script>
	$(document).ready(function() {
		$("a:has(img)").attr('rel', 'gallery').fancybox({
			'openEffect'	: 'elastic',
			'closeEffect'	: 'none',
			'beforeLoad' : function() { ""; }
		});
	});
    </script>
@stop