@extends('layout')

@section('header')
<title>Niller Records - Niller</title>
<meta name="description" content="Ida malt gerne, Phillie fährt seine Platten am liebsten mit dem Fahrrad durch die Gegend. Sie supporten Künstler, Produzenten, DJs und Plattensammler.">
<meta name="og:title" content="Is klar ne.">
<meta name="og:site-name" content="Niller Records">
<meta name="og:image" content="http://www.nillerrecords.com/images/about.png">
<meta name="og:type" content="blog">
<meta name="og:locale" content="de-DE">
<meta name="og:description" content="Ida malt gerne, Phillie fährt seine Platten am liebsten mit dem Fahrrad durch die Gegend und Leika steht auf Enten. Außerdem lieben und sammeln die drei Musik. Seit 2013 versorgen sie auch den Rest der Welt damit und supporten dabei verschiedene Künstler, Produzenten, DJs und Plattensammler.">
@stop

@section('content')
    <ul>
@foreach($artists as $artist)
    <li class="artist">
	<a class="element-link" href="{{ $artist->post_url() }}">
        <div>
	    @if ($artist->has_thumbnail())
                {{ $artist->thumbnail(array(170,170)) }}
            @else
                {{ HTML::image('images/about.png', $alt="No Image", $attributes = array("width" => 170, "height" => 170)) }}
            @endif
        </div>
    <p class="center">{{ $artist->title(20) }}</p>
	</a>
    </li>
@endforeach
    </ul>
@stop