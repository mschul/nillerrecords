@extends('layout')
@section('header')
<title>Niller Records - {{ $event->post_title }}</title>
<meta name="description" content="{{ $event->excerpt(15) }}">
<meta name="og:title" content="{{ $event->post_title }}">
<meta name="og:site-name" content="Niller Records">
@if ($event->has_thumbnail())
<meta name="og:image" content="{{ $event->thumbnail_url() }}">
@else
<meta name="og:image" content="http://www.nillerrecords.com/images/about.png">
@endif
<meta name="og:type" content="blog">
<meta name="og:locale" content="de-DE">
<meta name="og:description" content="{{ $event->excerpt(30) }}">
@stop

@section('content')
    <div class="post-entry">
    <h2>{{ $event->post_title }}</h2>
    <p class="date">{{ date("j. m. Y",strtotime($event->meta->event_date)) }}</p>
    <p>
	{{ $event->content() }}
    </p>
    <em>
	{{ $event->meta->event_location }} @ {{ $event->meta->event_city }}
	{{ $event->meta->event_start_time }}
	{{ link_to($event->meta->event_location_url) }}
    </em>
    </div>
    </a>
    <!-- start fancybox -->
    <script>
	$(document).ready(function() {
		$("a:has(img)").attr('rel', 'gallery').fancybox({
			'openEffect'	: 'elastic',
			'closeEffect'	: 'none',
			'beforeLoad' : function() { this.title = ""; }
		});
	});
    </script>
@stop