@extends('layout')
@section('header')
<title>Niller Records - Veranstaltungen</title>
<meta name="description" content="{{ $events[0]->excerpt(15) }}">
<meta name="og:title" content="{{ $events[0]->post_title }}">
<meta name="og:site-name" content="Niller Records">
@if ($events[0]->has_thumbnail())
<meta name="og:image" content="{{ $events[0]->thumbnail_url() }}">
@else
<meta name="og:image" content="http://www.nillerrecords.com/images/about.png">
@endif
<meta name="og:type" content="blog">
<meta name="og:locale" content="de-DE">
<meta name="og:description" content="{{ $events[0]->excerpt(30) }}">
@stop

@section('content')
    <ul>
@foreach($events as $event)
    <li class="post-entry">
	<a class="element-link" href="{{ $event->post_url() }}">
        <div class="post-thumb">
            @if ($event->has_thumbnail())
                {{ $event->thumbnail(array(300,200)) }}
            @else
                {{ HTML::image('images/about.png', $alt="No Image", $attributes = array("width" => 300, "height" => 200)) }}
            @endif
        </div>
    <h2>{{ $event->post_title }}</h2>
    </a>
	
    <p class="date">{{ date("j. m. Y",strtotime($event->meta->event_date)) }}</p>
    <p>
	{{ $event->excerpt(23) }}
    </p>
    <em>
	{{ $event->meta->event_location }} @ {{ $event->meta->event_city }}
	{{ $event->meta->event_start_time }}
	{{ link_to($event->meta->event_location_url) }}
    </em>
    
    <div class="clear"></div>
    </li>
@endforeach
    </ul>
@stop