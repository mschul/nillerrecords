<!doctype html> 
<html>
    <head>
		<link href="//fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet" type="text/css">
        <!-- Add jQuery library -->
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
        <!-- Add jScroll library -->
        <script type="text/javascript" src="/jscroll/jquery.jscroll.min.js"></script>
        <!--<script type="text/javascript" src="jscroll/jquery.jscroll.js"></script>-->
        <!-- Add fancyBox -->
        <script type="text/javascript" src="/scripts/jmp.js"></script>
        <link rel="stylesheet" href="/scripts/source/jf.css" type="text/css" media="screen" />
        <script type="text/javascript" src="/scripts/source/jfp.js"></script>
		<meta name="og:url" content="{{Request::url()}}">
		{{ HTML::style('css/release.css'); }}
		@yield('header')
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57231689-1', 'auto');
  ga('send', 'pageview');
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-57231689-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
    </head>
    <body>
        <div class="wrapper">
	    <div class="banner-container">
		<a href="{{ url('/') }}"><div class="logo"></div></a>
		@include('navigation')
	    </div>
	    
	    <div class="content-wrapper">
		<div class="content-container">@yield('content')</div>
		<div class="sidebar-container">@include('sidebar')</div>
	    </div>
	    <div class="clear"></div>
        </div>
    </body>
</html>