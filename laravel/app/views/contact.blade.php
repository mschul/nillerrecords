@extends('layout')
@section('header')
<title>Niller Records - Kontakt</title>
<meta name="description" content="Ida malt gerne, Phillie fährt seine Platten am liebsten mit dem Fahrrad durch die Gegend. Sie supporten Künstler, Produzenten, DJs und Plattensammler.">
<meta name="og:title" content="Is klar ne.">
<meta name="og:site-name" content="Niller Records">
<meta name="og:image" content="http://www.nillerrecords.com/images/about.png">
<meta name="og:type" content="blog">
<meta name="og:locale" content="de-DE">
<meta name="og:description" content="Ida malt gerne, Phillie fährt seine Platten am liebsten mit dem Fahrrad durch die Gegend und Leika steht auf Enten. Außerdem lieben und sammeln die drei Musik. Seit 2013 versorgen sie auch den Rest der Welt damit und supporten dabei verschiedene Künstler, Produzenten, DJs und Plattensammler.">
@stop

@section('content')
    <h2 class="center">Kontakt</h2>
	<br/>
	<p class="center">Für Bookings und Fragen<br/>
schick uns doch einfach ne Mail an</p>
<p class="center"><a class="element-link-2" href="mailto:nillerrecords@gmx.de">nillerrecords@gmx.de</a></p>
@stop