<?php

use Illuminate\Support\Collection;
use Niller\Storage\PostRepository as Post;

class PostController extends \BaseController {
	
	public function __construct(Post $post)
	{
	  $this->post = $post;
	}
	
        public function index()
        {
            $data = $this->post->index(Input::get('page', 1), 5);
                
	    if(!empty($data->items))
	    {
		$posts = Paginator::make($data->items, $data->totalItems, 5);
		return View::make('posts.index', compact('posts'));
	    }
            
            App::abort(404);
        }
        public function indexY($yyyy)
        {
            return $this->indexByDate($yyyy);
        }
        public function indexMY($mm, $yyyy)
        {
            return $this->indexByDate($yyyy, $mm);
        }
        public function indexDMY($dd, $mm, $yyyy)
        {
            return $this->indexByDate($yyyy, $mm, $dd);
        }
        
	public function indexByTag($name)
        {
            $data = $this->post->indexByTag($name, Input::get('page', 1), 5);
                
	    if(!empty($data->items))
	    {
		$posts = Paginator::make($data->items, $data->totalItems, 5);
		return View::make('posts.index', compact('posts'));
	    }
            
            App::abort(404);
	}

        public function view($dd, $mm, $yyyy, $name)
        {
	    $post = $this->post->view($dd, $mm, $yyyy, $name);
	    if($post)    
		return View::make('posts.view', compact('post'));
            
            App::abort(404);
        }
	 
        private function indexByDate($yyyy, $mm = -1, $dd = -1)
        {
            $data = $this->post->indexByDate($dd, $mm, $yyyy, Input::get('page', 1), 5);
	    
	    if(!empty($data->items))
	    {
		$posts = Paginator::make($data->items, $data->totalItems, 5);
		return View::make('posts.index', compact('posts'));
	    }
            
            App::abort(404);
        }
}
