<?php

use Illuminate\Support\Collection;
use Niller\Storage\LadenRepository as Laden;

class LadenController extends \BaseController {
	
	public function __construct(Laden $laden)
	{
	  $this->laden = $laden;
	}
	
        public function index()
        {
	    $l = $this->laden->index();
            
            if($l->count())    
                return View::make('laden', compact('l'));
            
            App::abort(404);
        }
}
