<?php

use Illuminate\Support\Collection;
use Niller\Storage\NillerRepository as NillerRepo;

class NillerController extends \BaseController {
	
	public function __construct(NillerRepo $niller)
	{
	  $this->niller = $niller;
	}
	
        public function index()
        {
	  $nillers = $this->niller->index();
	  if($nillers->count())
                return View::make('niller.index', compact('nillers'));
            
            App::abort(404);
        }
        
        public function view($name)
        {
            $niller = $this->niller->view($name);
            if($niller)
                return View::make('niller.view', compact('niller'));
            
            App::abort(404);
        }
}
