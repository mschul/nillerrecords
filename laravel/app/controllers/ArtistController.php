<?php

use Illuminate\Support\Collection;
use Niller\Storage\ArtistRepository as Artist;

class ArtistController extends \BaseController {
	
	public function __construct(Artist $artist)
	{
	  $this->artist = $artist;
	}
	
        public function index()
        {
	  $artists = $this->artist->index();
	  if($artists->count())
                return View::make('artists.index', compact('artists'));
            
            App::abort(404);
        }
        
        public function view($name)
        {
            $artist = $this->artist->view($name);
            if($artist)
                return View::make('artists.view', compact('artist'));
            
            App::abort(404);
        }
}
