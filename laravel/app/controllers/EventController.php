<?php

use Illuminate\Support\Collection;
use Niller\Storage\EventRepository as Event;

class EventController extends \BaseController {
	
	public function __construct(Event $event)
	{
	  $this->event = $event;
	}
	
        public function index()
        {
	    $events = $this->event->index();
            
            if($events->count())    
                return View::make('events.index', compact('events'));
            
            App::abort(404);
        }
        
        public function view($id)
        {
            $event = $this->event->view($id);
        
            if($event)    
                return View::make('events.view', compact('event'));
            
            App::abort(404);
        }
}
