<?php namespace Niller\Storage;
 
interface EventRepository {
    
    public function index();
    public function indexFuture();
    public function view($id);
 
}