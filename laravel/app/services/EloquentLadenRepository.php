<?php namespace Niller\Storage;
use Niller\Models\WordpressPost as WordpressPost;
 
class EloquentLadenRepository implements LadenRepository
{
    public function index()
    {
        return WordpressPost::type(array('laden'))->status('publish')->first();
    }
}