<?php namespace Niller\Storage;
 
interface NillerRepository {
    
    public function index();
    public function view($name);
 
}
