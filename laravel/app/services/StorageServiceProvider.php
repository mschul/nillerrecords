<?php namespace Niller\Storage;
 
use Illuminate\Support\ServiceProvider;

use Niller\Storage\LaravelCache;

use Niller\Storage\PostCacheDecorator;
use Niller\Storage\EventCacheDecorator;
use Niller\Storage\ArtistCacheDecorator;
use Niller\Storage\NillerCacheDecorator;

use Niller\ViewComposers\AboutViewComposer as AboutViewComposer;
use Niller\ViewComposers\TaglistViewComposer as TaglistViewComposer;
use Niller\ViewComposers\EventlistViewComposer as EventlistViewComposer;
 
class StorageServiceProvider extends ServiceProvider {
 
  public function register()
  {
    $this->app->bind('Niller\Storage\PostRepository', function($app)
    {
      return new PostCacheDecorator(
        new EloquentPostRepository(),
        new LaravelCache($app['cache'], 'post')
      );
    });
    $this->app->bind('Niller\Storage\EventRepository', function($app)
    {
      return new EventCacheDecorator(
        new EloquentEventRepository(),
        new LaravelCache($app['cache'], 'event')
      );
    });
    $this->app->bind('Niller\Storage\NillerRepository', function($app)
    {
      return new NillerCacheDecorator(
        new EloquentNillerRepository(),
        new LaravelCache($app['cache'], 'niller')
      );
    });
    $this->app->bind('Niller\Storage\ArtistRepository', function($app)
    {
      return new ArtistCacheDecorator(
        new EloquentArtistRepository(),
        new LaravelCache($app['cache'], 'artist')
      );
    });
    $this->app->bind('Niller\Storage\LadenRepository', function($app)
    {
      return new LadenCacheDecorator(
        new EloquentLadenRepository(),
        new LaravelCache($app['cache'], 'laden')
      );
    });
    $this->app->bind('TaglistViewComposer', function($app)
    {
      new TaglistViewComposer($this->app->make('Niller\Storage\PostRepository'));
    });
    $this->app->bind('EventlistViewComposer', function($app)
    {
      new EventlistViewComposer($this->app->make('Niller\Storage\EventRepository'));
    });
    $this->app->bind('AboutViewComposer', function($app)
    {
      new AboutViewComposer($this->app->make('Niller\Storage\PostRepository'));
    });
  }
 
}
