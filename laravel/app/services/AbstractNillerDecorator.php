<?php namespace Niller\Storage;
 
abstract class AbstractNillerDecorator implements NillerRepository {
 
  /**
   * @var UserRepository
   */
  protected $niller;
  public function __construct(NillerRepository $niller)
  {
    $this->niller = $niller;
  }
 
  public function index()
  {
    return $this->niller->index();
  }
  public function view($name)
  {
    return $this->niller->view($name);
  }
 
}
