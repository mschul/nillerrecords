<?php namespace Niller\Storage;
 
interface ArtistRepository {
    
    public function index();
    public function view($name);
 
}