<?php namespace Niller\Storage;
 
use Niller\Service\Cache\CacheInterface;
use stdClass;
 
class PostCacheDecorator extends AbstractPostDecorator {
 
  protected $cache;
 
  public function __construct(PostRepository $post, CacheInterface $cache)
  {
    parent::__construct($post);
    $this->cache = $cache;
  }
 
 /*
  public function index($page, $limit)
  {
    $key = md5('page'.$page.'limit'.$limit);

    if($this->cache->has($key))
    {
      return $this->cache->get($key);
    }
 
    $posts = $this->post->index($page, $limit);
    
    $this->cache->put($key, $posts);
 
    return $posts;
  }
  
  public function indexByDate($dd, $mm, $yyyy, $page, $limit)
  {
    $key = md5('dd.'.$dd.'mm'.$mm.'yyyy'.$yyyy.'page'.$page.'limit'.$limit);

    if($this->cache->has($key))
    {
      return $this->cache->get($key);
    }
    
    $posts = $this->post->indexByDate($dd, $mm, $yyyy, $page, $limit);
    
    $this->cache->put($key, $posts);
 
    return $posts;
  }
  
  public function view($dd, $mm, $yyyy, $name)
  {
    $key = md5('dd.'.$dd.'mm'.$mm.'yyyy'.$yyyy.'name'.$name);
 
    if($this->cache->has($key))
    {
      return $this->cache->get($key);
    }
 
    $post = $this->post->view($dd, $mm, $yyyy, $name);
 
    $this->cache->put($key, $post);
 
    return $post;
  }
  
  public function getAbout()
  {
    $key = md5('getAbout');
 
    if($this->cache->has($key))
    {
      return $this->cache->get($key);
    }
 
    $post = $this->post->getAbout();
 
    $this->cache->put($key, $post);
 
    return $post;
  }
  */
 
}