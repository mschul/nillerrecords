<?php namespace Niller\Storage;
 
use Niller\Service\Cache\CacheInterface;
 
class ArtistCacheDecorator extends AbstractArtistDecorator {
 
  protected $cache;
 
  public function __construct(ArtistRepository $artist, CacheInterface $cache)
  {
    parent::__construct($artist);
    $this->cache = $cache;
  }
 /*
  public function index()
  {
    $key = md5('index');

    if($this->cache->has($key))
    {
      return $this->cache->get($key);
    }
 
    $artist = $this->artist->index();
 
    $this->cache->put($key, $artist);
 
    return $artist;
  }
 
  public function view($name)
  {
    $key = md5('name.'.$name);
 
    if($this->cache->has($key))
    {
      return $this->cache->get($key);
    }
 
    $artist = $this->artist->view($name);
 
    $this->cache->put($key, $artist);
 
    return $artist;
  }
  */
 
}