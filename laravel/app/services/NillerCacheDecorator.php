<?php namespace Niller\Storage;
 
use Niller\Service\Cache\CacheInterface;
 
class NillerCacheDecorator extends AbstractNillerDecorator {
 
  protected $cache;
 
  public function __construct(NillerRepository $niller, CacheInterface $cache)
  {
    parent::__construct($niller);
    $this->cache = $cache;
  }
 /*
  public function index()
  {
    $key = md5('index');

    if($this->cache->has($key))
    {
      return $this->cache->get($key);
    }
 
    $niller = $this->niller->index();
 
    $this->cache->put($key, $niller);
 
    return $niller;
  }
 
  public function view($name)
  {
    $key = md5('name.'.$name);
 
    if($this->cache->has($key))
    {
      return $this->cache->get($key);
    }
 
    $niller = $this->niller->view($name);
 
    $this->cache->put($key, $niller);
 
    return $niller;
  }
  */
 
}
