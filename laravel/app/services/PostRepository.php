<?php namespace Niller\Storage;
 
interface PostRepository {
    
    public function index($page, $limit);
    public function indexByDate($dd, $mm, $yyyy, $page, $limit);
    public function indexByTag($name, $page, $limit);
    public function view($dd, $mm, $yyyy, $name);
    public function getAbout();
    public function getTags(); 
}
