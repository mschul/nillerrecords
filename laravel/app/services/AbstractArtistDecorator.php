<?php namespace Niller\Storage;
 
abstract class AbstractArtistDecorator implements ArtistRepository {
 
  /**
   * @var UserRepository
   */
  protected $artist;
  public function __construct(ArtistRepository $artist)
  {
    $this->artist = $artist;
  }
 
  public function index()
  {
    return $this->artist->index();
  }
  public function view($name)
  {
    return $this->artist->view($name);
  }
 
}