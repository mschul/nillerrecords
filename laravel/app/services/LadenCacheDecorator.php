<?php namespace Niller\Storage;
 
use Niller\Service\Cache\CacheInterface;
 
class LadenCacheDecorator extends AbstractLadenDecorator {
 
  protected $cache;
 
  public function __construct(LadenRepository $laden, CacheInterface $cache)
  {
    parent::__construct($laden);
    $this->cache = $cache;
  }
 /*
  public function index()
  {
    $key = md5('index');

    if($this->cache->has($key))
    {
      return $this->cache->get($key);
    }
 
    $laden = $this->laden->index();
 
    $this->cache->put($key, $laden);
 
    return $laden;
  }
 */
}