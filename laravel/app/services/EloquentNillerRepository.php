<?php namespace Niller\Storage;
use Niller\Models\NillerModel as NillerModel;

class EloquentNillerRepository implements NillerRepository
{    
    public function index()
    {
        return NillerModel::status('publish')->orderby('post_title', 'asc')->get();
    }
    public function view($name)
    {
        return NillerModel::status('publish')->where('post_title', '=', $name)->first();
    }
 
}
