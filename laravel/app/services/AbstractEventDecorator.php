<?php namespace Niller\Storage;
 
abstract class AbstractEventDecorator implements EventRepository {
 
  /**
   * @var UserRepository
   */
  protected $event;
  public function __construct(EventRepository $event)
  {
    $this->event = $event;
  }
  
  public function index()
  {
    return $this->event->index();
  }
  public function indexFuture()
  {
    return $this->event->indexFuture();
  }
  public function view($id)
  {
    return $this->event->view($id);
  }
 
}