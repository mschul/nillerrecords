<?php namespace Niller\Storage;
use Illuminate\Database\Eloquent\Collection;
use Niller\Models\WordpressPost as WordpressPost;
use Niller\Models\Post as Post;
use Niller\Models\Term as Term;
use Niller\Models\About as About;
use Niller\Models\TermTaxonomy as TermTaxonomy;
use stdClass;
use DateTime;
 
class EloquentPostRepository implements PostRepository
{
    
    public function index($page, $limit)
    {
        $model = Post::status('publish')
                ->orderby('post_date', 'desc');
        return $this->getByPage($model, $page, $limit); 
    }
    public function indexByTag($name, $page, $limit)
    {
	$term = TermTaxonomy::slug($name)->firstOrFail();
	$model = $term->posts()->orderby('post_date', 'desc');
	return $this->getByPage($model, $page, $limit);
    }
    public function indexByDate($dd, $mm, $yyyy, $page, $limit)
    {
        $minDate = new DateTime(($dd > 0 ? $dd : '01').'-'.($mm > 0 ? $mm : '01').'-'.$yyyy.' 00:00:00');
        $maxDate = new DateTime(($dd > 0 ? $dd : '31').'-'.($mm > 0 ? $mm : '12').'-'.$yyyy.' 23:59:59');
            
        $model = WordpressPost::multitype(array('post', 'events'))->status('publish')
            ->inDateInterval($minDate, $maxDate)->orderby('post_date', 'desc');
        
        return $this->getByPage($model, $page, $limit); 
    }
    public function view($dd, $mm, $yyyy, $name)
    {
        $minDate = new DateTime($dd.'-'.$mm.'-'.$yyyy.' 00:00:00');
        $maxDate = new DateTime($dd.'-'.$mm.'-'.$yyyy.' 23:59:59');
        
        return Post::status('publish')->where('post_title', "=", $name)
            ->inDateInterval($minDate, $maxDate)->first();
    }
    public function getAbout()
    {
        return About::status('publish')->first();
    }
    public function getTags()
    {
	return TermTaxonomy::tag()->where('count', '>', 0)->orderby('count', 'desc')->get();
    }    

    private function getByPage($model, $page, $limit)
    {
        $results = new stdClass;
        $results->page = $page;
        $results->limit = $limit;
        $results->totalItems = $model->count();
        $results->items = $model->skip($limit * ($page - 1))->take($limit)->get()->all();
       
        return $results;
    }

}
