<?php namespace Niller\Storage;
 
abstract class AbstractPostDecorator implements PostRepository {
 
  /**
   * @var UserRepository
   */
  protected $post;
  public function __construct(PostRepository $post)
  {
    $this->post = $post;
  }
 
  public function index($page, $limit)
  {
    return $this->post->index($page, $limit);
  }
  public function indexByDate($dd, $mm, $yyyy, $page, $limit)
  {
    return $this->post->indexByDate($dd, $mm, $yyyy, $page, $limit);
  }
  public function indexByTag($name, $page, $limit)
  {
    return $this->post->indexByTag($name, $page, $limit);
  }
  public function view($dd, $mm, $yyyy, $name)
  {
    return $this->post->view($dd, $mm, $yyyy, $name);
  }
  public function getAbout()
  {
    return $this->post->getAbout();
  }
  public function getTags()
  {
    return $this->post->getTags();
  }
 
}
