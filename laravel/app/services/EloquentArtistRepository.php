<?php namespace Niller\Storage;
use Niller\Models\Artist as Artist;

class EloquentArtistRepository implements ArtistRepository
{    
    public function index()
    {
        return Artist::status('publish')->orderby('post_title', 'asc')->get();
    }
    public function view($name)
    {
        return Artist::status('publish')->where('post_title', '=', $name)->first();
    }
 
}