<?php namespace Niller\Storage;
 
abstract class AbstractLadenDecorator implements LadenRepository {
 
  /**
   * @var UserRepository
   */
  protected $laden;
  public function __construct(LadenRepository $laden)
  {
    $this->laden = $laden;
  }
 
  public function index()
  {
    return $this->laden->index();
  }
 
}