<?php namespace Niller\Storage;
 
use Niller\Service\Cache\CacheInterface;
 
class EventCacheDecorator extends AbstractEventDecorator {
 
  protected $cache;
 
  public function __construct(EventRepository $event, CacheInterface $cache)
  {
    parent::__construct($event);
    $this->cache = $cache;
  }
 /*
  public function index()
  {
    $key = md5('index');

    if($this->cache->has($key))
    {
      return $this->cache->get($key);
    }
 
    $events = $this->event->index();
 
    $this->cache->put($key, $events);
 
    return $events;
  }
  
  public function indexFuture()
  {
    $key = md5('indexFuture');

    if($this->cache->has($key))
    {
      return $this->cache->get($key);
    }
 
    $events = $this->event->indexFuture();
 
    $this->cache->put($key, $events);
 
    return $events;
  }
 
  public function view($id)
  {
    $key = md5('id.'.$id);
 
    if($this->cache->has($key))
    {
      return $this->cache->get($key);
    }
 
    $event = $this->event->view($id);
 
    $this->cache->put($key, $event);
 
    return $event;
  }
 */
}