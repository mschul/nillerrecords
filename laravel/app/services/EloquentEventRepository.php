<?php namespace Niller\Storage;

use Illuminate\Database\Eloquent\Collection;
use Niller\Models\Event as Event;
use Niller\Models\PostMeta as PostMeta;
 
class EloquentEventRepository implements EventRepository
{    
    public function index()
    {
        $eventmetas = PostMeta::where('meta_key', '=', 'event_date')->orderby('meta_value', 'desc')->get();
        $events = new Collection;
        foreach($eventmetas as $e)
        {
            $post = $e->post;
            if($post->post_status == 'publish')
                $events->push($post);
        }
        return $events;
    }
    public function indexFuture()
    {       
        $eventlist = new Collection;
        $eventmetas = PostMeta::where('meta_key', '=', 'event_date')->orderby('meta_value', 'asc')->get();
        foreach($eventmetas as $e)
        {
          $post = $e->post;
          if((time()-(60*60*24)) < strtotime($post->meta->event_date) && $post->post_status == 'publish')
            $eventlist->push($e->post);
        }
        return $eventlist;
    }
    public function view($id)
    {
        return Event::status('publish')->where('ID', "=", $id)->first();
    }
 
}